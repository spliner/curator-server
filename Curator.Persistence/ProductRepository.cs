﻿using Curator.Model;
using Curator.Persistence.API;
using Curator.Persistence.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Curator.Persistence
{
    public class ProductRepository : IProductRepository
    {
        private readonly CuratorDbContext context;

        public ProductRepository(CuratorDbContext context)
        {
            this.context = context;
        }

        public async Task<Product[]> Search(int skip, int take, string query)
        {
            var result = context.Products.AsQueryable();
            if (!string.IsNullOrEmpty(query))
                result = result.Where(p => p.Name.StartsWith(query) || p.Code.StartsWith(query));
            return await result.Skip(skip).Take(take).Select(p => p.ToModel()).ToArrayAsync();
        }

        public async Task<Product> FindById(string id)
        {
            var product = await context.Products.FindAsync(id);
            return product?.ToModel();
        }

        public async Task<Product> FindByCode(string code)
        {
            var product = await context.Products.FirstOrDefaultAsync(p => p.Code == code);
            return product?.ToModel();
        }

        public async Task<Product> Create(Product model)
        {
            var product = model.FromModel();
            await context.Products.AddAsync(product);
            await context.SaveChangesAsync();
            return product.ToModel();
        }

        public async Task Update(Product model)
        {
            var existingProduct = await FindById(model.Id);
            if (existingProduct == null)
                throw new ArgumentException($"Could not find Product with Id {model.Id}");

            var product = model.FromModel();
            var entry = context.Entry(product);
            entry.State = EntityState.Modified;

            await context.SaveChangesAsync();
        }
    }
}
