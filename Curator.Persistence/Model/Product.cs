﻿using System.Collections.Generic;

namespace Curator.Persistence.Model
{
    public class Product
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public ICollection<UserProduct> UserProducts { get; set; }

        public Product()
        {
            UserProducts = new List<UserProduct>();
        }
    }
}
