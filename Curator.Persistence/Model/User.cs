﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Curator.Persistence.Model
{
    public class User : IdentityUser
    {
        public const string UserRoleName = "User";
        public const string AdminRoleName = "Admin";

        public string Name { get; set; }
        public ICollection<UserProduct> UserProducts { get; set; }

        public User()
        {
            UserProducts = new List<UserProduct>();
        }

        public override int GetHashCode()
        {
            return (Id ?? "").GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is User other))
                return false;
            return other.Id == Id;
        }
    }
}
