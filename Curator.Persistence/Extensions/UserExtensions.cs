﻿using Curator.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Curator.Persistence.Extensions
{
    internal static class UserExtensions
    {
        internal static User ToModel(this Model.User user, IEnumerable<string> roles)
        {
            var role = roles.Aggregate(
                (Role?)null,
                (currRole, roleName) =>
                {
                    if (!Enum.TryParse(roleName, out Role newRole))
                        return currRole;
                    if (currRole == null)
                        return newRole;
                    return currRole | newRole;
                });

            return new User
            {
                Id = user.Id,
                Name = user.Name,
                Username = user.UserName,
                Email = user.Email,
                Role = role ?? Role.Default
            };
        }

        internal static Model.User FromModel(this User user)
        {
            return new Model.User
            {
                Id = user.Id,
                Name = user.Name,
                UserName = user.Username,
                Email = user.Email
            };
        }
    }
}
