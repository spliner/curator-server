﻿using Curator.Model;

namespace Curator.Persistence.Extensions
{
    internal static class ProductExtensions
    {
        internal static Product ToModel(this Model.Product product)
        {
            return new Product
            {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name
            };
        }

        internal static Model.Product FromModel(this Product model)
        {
            return new Model.Product
            {
                Id = model.Id,
                Code = model.Code,
                Name = model.Name
            };
        }
    }
}
