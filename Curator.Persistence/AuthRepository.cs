﻿using Curator.Persistence.API;
using Curator.Persistence.Extensions;
using Curator.Persistence.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Curator.Persistence
{
    public class AuthRepository : IAuthRepository
    {
        private readonly CuratorDbContext context;
        private readonly UserManager<User> userManager;

        public AuthRepository(CuratorDbContext context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        // TODO: Throw a better Exception
        public async Task Register(Curator.Model.User user, string password)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                var persistenceUser = user.FromModel();
                var createResult = await userManager.CreateAsync(persistenceUser, password);
                if (!createResult.Succeeded)
                    throw new AggregateException(createResult.Errors.Select(e => new Exception(e.Description)));

                var roleResult = await userManager.AddToRoleAsync(persistenceUser, User.UserRoleName);
                if (!roleResult.Succeeded)
                    throw new AggregateException(roleResult.Errors.Select(e => new Exception(e.Description)));

                await context.SaveChangesAsync();
                transaction.Commit();
            }
        }

        public async Task<Curator.Model.User> Login(string username, string password)
        {
            var user = await context.Users.AsNoTracking().SingleOrDefaultAsync(u => u.UserName == username);
            if (user == null)
                return null;

            var passwordValid = await userManager.CheckPasswordAsync(user, password);
            if (!passwordValid)
                return null;

            // TODO: Get user and roles in a single query
            var roles = await userManager.GetRolesAsync(user);
            return user.ToModel(roles);
        }
    }
}
