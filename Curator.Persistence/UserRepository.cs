﻿using Curator.Persistence.API;
using Curator.Persistence.Extensions;
using Curator.Persistence.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Curator.Persistence
{
    public class UserRepository : IUserRepository
    {
        private readonly CuratorDbContext context;
        private readonly UserManager<User> userManager;

        public UserRepository(CuratorDbContext context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public async Task<Curator.Model.User> FindById(string id)
        {
            var user = await context.Users.FindAsync(id);
            if (user == null)
                return null;

            var roles = await userManager.GetRolesAsync(user);
            return user?.ToModel(roles);
        }

        public async Task<Curator.Model.Product[]> FindProducts(string userId, int skip, int take)
        {
            return await context.UserProducts
                .Where(u => u.UserId == userId)
                .Skip(skip)
                .Take(take)
                .Select(u => u.Product.ToModel())
                .AsNoTracking()
                .ToArrayAsync();
        }
    }
}
