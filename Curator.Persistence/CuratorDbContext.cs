﻿using Curator.Persistence.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Curator.Persistence
{
    public class CuratorDbContext : IdentityDbContext<User>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<UserProduct> UserProducts { get; set; }

        public CuratorDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().HasAlternateKey(u => u.UserName);
            builder.Entity<User>().Property(u => u.Email).IsRequired();

            builder.Entity<UserProduct>().HasKey(u => new { u.UserId, u.ProductId });

            builder.Entity<UserProduct>()
                .HasOne(u => u.User)
                .WithMany(u => u.UserProducts)
                .HasForeignKey(u => u.UserId);

            builder.Entity<UserProduct>()
                .HasOne(u => u.Product)
                .WithMany(u => u.UserProducts)
                .HasForeignKey(u => u.ProductId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.ConfigureWarnings(a =>
            {
                a.Ignore(InMemoryEventId.TransactionIgnoredWarning);
            });
        }
    }
}
