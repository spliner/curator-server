﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Core.API
{
    public interface IUserService
    {
        Task<User> FindById(string id);
        Task<Page<Product>> FindProducts(string userId, int skip, int take);
    }
}
