﻿using Curator.Model;

namespace Curator.Core.API.Auth
{
    public interface ITokenFactory
    {
        string GenerateToken(User user);
    }
}
