﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Core.API
{
    public interface IProductService
    {
        Task<Page<Product>> Search(int skip, int take, string query);
        Task<Product> FindById(string id);
        Task<Product> FindByCode(string code);
        Task<Product> Create(Product product);
        Task Update(Product product);
    }
}
