﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Core.API
{
    public interface IAuthService
    {
        Task Register(User user, string password);
        Task<User> Login(string username, string password);
    }
}
