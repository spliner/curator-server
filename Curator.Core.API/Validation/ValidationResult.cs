﻿using System.Collections.Generic;
using System.Linq;

namespace Curator.Core.API.Validation
{
    public class ValidationResult
    {
        public IEnumerable<ValidationError> Errors { get; set; }

        public bool Succeeded => !Errors.Any();

        public ValidationResult()
        {
            Errors = new ValidationError[0];
        }

        public void ThrowForError()
        {
            if (!Succeeded)
                throw new ValidationException(Errors);
        }

        public override string ToString()
        {
            return Succeeded ? "Succeeded" : "Error";
        }
    }
}
