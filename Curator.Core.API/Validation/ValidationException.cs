﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Curator.Core.API.Validation
{
    public class ValidationException : Exception
    {
        public ValidationException(IEnumerable<ValidationError> errors)
            : base(string.Join("\n", errors.Select(e => e.Message)))
        {
        }
    }
}
