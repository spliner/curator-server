﻿using Curator.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Curator.Web
{
    public partial class Startup
    {
        private void SeedDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<CuratorDbContext>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<Persistence.Model.User>>();
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();

                if (!context.Users.Any())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        roleManager.CreateAsync(new IdentityRole(Persistence.Model.User.UserRoleName)).Wait();
                        roleManager.CreateAsync(new IdentityRole(Persistence.Model.User.AdminRoleName)).Wait();

                        var user = new Persistence.Model.User
                        {
                            Name = "I am root",
                            UserName = "root",
                            Email = "root@curator.zone"
                        };
                        userManager.CreateAsync(user, "Temp123#").Wait();
                        userManager.AddToRolesAsync(user, new[] { Persistence.Model.User.UserRoleName, Persistence.Model.User.AdminRoleName }).Wait();

                        var product1 = new Persistence.Model.Product
                        {
                            Code = "123",
                            Name = "Product 1",
                        };
                        var product2 = new Persistence.Model.Product
                        {
                            Code = "321",
                            Name = "Product 2",
                        };
                        context.Products.AddRange(new[] { product1, product2 });
                        context.SaveChanges();

                        var userProduct = new Persistence.Model.UserProduct
                        {
                            Product = product1,
                            User = user
                        };
                        context.UserProducts.Add(userProduct);
                        context.SaveChanges();

                        transaction.Commit();
                    }
                }
            }
        }
    }
}
