﻿using Curator.Core.API.Auth;
using Curator.Model;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Curator.Web.Auth
{
    public class JwtFactory : ITokenFactory
    {
        private readonly JwtIssuerOptions options;

        public JwtFactory(IOptions<JwtIssuerOptions> options)
        {
            this.options = options.Value;
        }

        public string GenerateToken(User user)
        {
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(options.IssuedAt).ToString()),
            };

            foreach (var role in user.RoleNames)
                claims.Add(new Claim(ClaimTypes.Role, role));

            var jwt = new JwtSecurityToken(
                issuer: options.Issuer,
                audience: options.Audience,
                claims: claims,
                expires: options.Expiration,
                signingCredentials: options.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        private long ToUnixEpochDate(DateTime date)
        {
            return (long)(date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds;
        }
    }
}
