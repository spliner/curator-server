﻿namespace Curator.Web.Dto
{
    public class PageDto<T>
    {
        public T[] Results { get; set; }
        public bool HasMore { get; set; }

        public PageDto()
        {
            Results = new T[0];
        }
    }
}
