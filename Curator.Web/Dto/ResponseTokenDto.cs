﻿namespace Curator.Web.Dto
{
    public class ResponseTokenDto
    {
        public string Token { get; set; }
    }
}
