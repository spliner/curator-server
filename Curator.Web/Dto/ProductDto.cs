﻿namespace Curator.Web.Dto
{
    public class ProductDto
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
