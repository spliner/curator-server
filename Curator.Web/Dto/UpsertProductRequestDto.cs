﻿namespace Curator.Web.Dto
{
    public class UpsertProductRequestDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
