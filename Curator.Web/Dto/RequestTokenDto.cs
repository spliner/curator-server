﻿namespace Curator.Web.Dto
{
    public class RequestTokenDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
