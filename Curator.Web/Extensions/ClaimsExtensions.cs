﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Curator.Web.Extensions
{
    internal static class ClaimsExtensions
    {
        internal static string GetId(this ClaimsPrincipal principal)
        {
            var claim = principal.FindFirst(JwtRegisteredClaimNames.Sub);
            return claim?.Value;
        }
    }
}
