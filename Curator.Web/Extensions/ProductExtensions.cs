﻿using Curator.Model;
using Curator.Web.Dto;

namespace Curator.Web.Extensions
{
    public static class ProductExtensions
    {
        public static ProductDto ToDto(this Product product)
        {
            return new ProductDto
            {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name
            };
        }

        public static Product FromDto(this UpsertProductRequestDto dto, string id = null)
        {
            return new Product
            {
                Id = id,
                Code = dto.Code,
                Name = dto.Name
            };
        }
    }
}
