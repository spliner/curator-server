﻿using Curator.Model;
using Curator.Web.Dto;
using System.Linq;

namespace Curator.Web.Extensions
{
    public static class UserExtensions
    {
        public static UserDto ToDto(this User user)
        {
            return new UserDto
            {
                Name = user.Name,
                Username = user.Username,
                Email = user.Email,
                Roles = user.RoleNames.ToArray()
            };
        }

        public static User FromDto(this CreateUserRequestDto dto)
        {
            return new User
            {
                Name = dto.Name,
                Username = dto.Username,
                Email = dto.Email
            };
        }
    }
}
