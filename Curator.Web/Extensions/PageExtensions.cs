﻿using Curator.Model;
using Curator.Web.Dto;
using System;
using System.Linq;

namespace Curator.Web.Extensions
{
    public static class PageExtensions
    {
        public static PageDto<TDto> ToDto<TDto, TModel>(this Page<TModel> page, Func<TModel, TDto> convertFunction)
        {
            return new PageDto<TDto>
            {
                Results = page.Results.Select(r => convertFunction(r)).ToArray(),
                HasMore = page.HasMore
            };
        }
    }
}
