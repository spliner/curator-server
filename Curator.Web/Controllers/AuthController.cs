﻿using Curator.Core.API;
using Curator.Core.API.Auth;
using Curator.Web.Dto;
using Curator.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Curator.Web.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthService authService;
        private readonly ITokenFactory tokenFactory;

        public AuthController(IAuthService authService, ITokenFactory tokenFactory)
        {
            this.authService = authService;
            this.tokenFactory = tokenFactory;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]CreateUserRequestDto dto)
        {
            var user = dto?.FromDto();
            await authService.Register(user, dto?.Password);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<IActionResult> RequestToken([FromBody] RequestTokenDto request)
        {
            if (request == null)
                return BadRequest();

            var user = await authService.Login(request.Username, request.Password);
            if (user == null)
                return BadRequest();

            var token = tokenFactory.GenerateToken(user);
            var response = new ResponseTokenDto
            {
                Token = token
            };
            return Ok(response);
        }
    }
}
