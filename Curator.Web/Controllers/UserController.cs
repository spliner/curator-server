﻿using Curator.Core.API;
using Curator.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Curator.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private const int DefaultSkip = 0;
        private const int DefaultTake = 10;

        private readonly IUserService service;

        public UserController(IUserService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUserInfo()
        {
            var userId = User.GetId();
            var user = await service.FindById(userId);
            if (user == null)
                return BadRequest();

            var dto = user.ToDto();
            return Ok(dto);
        }

        [Authorize]
        [HttpGet("products")]
        public async Task<IActionResult> GetProducts(int? skip, int? take)
        {
            var userId = User.GetId();
            var page = await service.FindProducts(userId, skip ?? DefaultSkip, take ?? DefaultTake);
            var dto = page.ToDto(p => p.ToDto());
            return Ok(dto);
        }
    }
}
