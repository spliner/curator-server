﻿using Curator.Core.API;
using Curator.Web.Dto;
using Curator.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Curator.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private const int DefaultSkip = 0;
        private const int DefaultTake = 10;

        private readonly IProductService service;

        public ProductsController(IProductService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> SearchProducts(int? skip, int? take, string query)
        {
            var page = await service.Search(skip ?? DefaultSkip, take ?? DefaultTake, query);
            var dto = page.ToDto(p => p.ToDto());
            return Ok(dto);
        }

        [Authorize]
        [HttpGet("{id}", Name = "GetProduct")]
        public async Task<IActionResult> FindById(string id)
        {
            var product = await service.FindById(id);
            if (product == null)
                return NotFound();

            var dto = product.ToDto();
            return Ok(dto);
        }

        [Authorize]
        [HttpGet("code/{code}")]
        public async Task<IActionResult> FindByCode(string code)
        {
            var product = await service.FindByCode(code);
            if (product == null)
                return NotFound();

            var dto = product.ToDto();
            return Ok(dto);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateProduct(UpsertProductRequestDto product)
        {
            var createdProduct = await service.Create(product?.FromDto());
            return CreatedAtRoute("GetProduct", new { id = createdProduct.Id }, createdProduct);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateProduct(string id, UpsertProductRequestDto product)
        {
            // TODO: 404 if Product does not exist
            await service.Update(product?.FromDto(id));
            return NoContent();
        }
    }
}
