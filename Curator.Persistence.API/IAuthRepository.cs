﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Persistence.API
{
    public interface IAuthRepository
    {
        Task Register(User user, string password);
        Task<User> Login(string username, string password);
    }
}
