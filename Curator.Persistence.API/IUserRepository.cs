﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Persistence.API
{
    public interface IUserRepository
    {
        Task<User> FindById(string id);
        Task<Product[]> FindProducts(string userId, int skip, int take);
    }
}
