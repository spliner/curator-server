﻿using Curator.Model;
using System.Threading.Tasks;

namespace Curator.Persistence.API
{
    public interface IProductRepository
    {
        Task<Product[]> Search(int skip, int take, string query);
        Task<Product> FindById(string id);
        Task<Product> FindByCode(string code);
        Task<Product> Create(Product product);
        Task Update(Product product);
    }
}
