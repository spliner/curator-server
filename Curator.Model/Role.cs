﻿using System;

namespace Curator.Model
{
    [Flags]
    public enum Role
    {
        Default,
        User,
        Admin
    }
}
