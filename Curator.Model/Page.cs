﻿namespace Curator.Model
{
    public class Page<T>
    {
        public T[] Results { get; set; }
        public bool HasMore { get; set; }

        public Page()
        {
            Results = new T[0];
        }
    }
}
