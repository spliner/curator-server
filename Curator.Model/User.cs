﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Curator.Model
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }

        public IEnumerable<string> RoleNames
        {
            get => Enum.GetValues(typeof(Role))
                .Cast<Role>()
                .Where(r => Role.HasFlag(r))
                .Select(r => r.ToString());
        }

        public User()
        {
            Role = Role.Default;
        }

        public override int GetHashCode()
        {
            return (Id ?? "").GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var other = obj as User;
            if (other == null)
                return false;
            return other.Id == Id;
        }
    }
}
