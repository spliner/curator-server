﻿using Curator.Core.API;
using Curator.Core.Validation;
using Curator.Model;
using Curator.Persistence.API;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Curator.Core
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository repository;

        public ProductService(IProductRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Page<Product>> Search(int skip, int take, string query)
        {
            // Look for one more item than requested so we can determine if there are more records
            var products = await repository.Search(skip, take + 1, query);
            return new Page<Product>
            {
                Results = products.Take(take).ToArray(),
                HasMore = products.Length > take
            };
        }

        public async Task<Product> FindById(string id)
        {
            return await repository.FindById(id);
        }

        public async Task<Product> FindByCode(string code)
        {
            return await repository.FindByCode(code);
        }

        public async Task<Product> Create(Product product)
        {
            var result = ProductValidator.ValidateForCreation(product);
            result.ThrowForError();
            return await repository.Create(product);
        }

        public async Task Update(Product product)
        {
            var validationResult = ProductValidator.ValidateForUpdate(product);
            validationResult.ThrowForError();
            await repository.Update(product);
        }
    }
}
