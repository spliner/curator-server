﻿using Curator.Core.API.Validation;
using Curator.Model;
using System.Collections.Generic;

namespace Curator.Core.Validation
{
    internal static class ProductValidator
    {
        internal static ValidationResult ValidateForCreation(Product product)
        {
            var errors = new List<ValidationError>();
            if (product == null)
                errors.Add(new ValidationError($"{nameof(product)} cannot be null"));
            if (string.IsNullOrWhiteSpace(product.Name))
                errors.Add(new ValidationError($"{nameof(product.Name)} cannot be empty"));
            if (string.IsNullOrWhiteSpace(product.Code))
                errors.Add(new ValidationError($"{nameof(product.Code)} cannot be empty"));
            return new ValidationResult { Errors = errors };
        }

        internal static ValidationResult ValidateForUpdate(Product product)
        {
            var errors = new List<ValidationError>();
            if (product == null)
                errors.Add(new ValidationError($"{nameof(product)} cannot be null"));
            if (string.IsNullOrWhiteSpace(product.Id))
                errors.Add(new ValidationError($"{nameof(product.Id)} cannot be empty"));
            if (string.IsNullOrWhiteSpace(product.Name))
                errors.Add(new ValidationError($"{nameof(product.Name)} cannot be empty"));
            if (string.IsNullOrWhiteSpace(product.Code))
                errors.Add(new ValidationError($"{nameof(product.Code)} cannot be empty"));
            return new ValidationResult { Errors = errors };
        }
    }
}
