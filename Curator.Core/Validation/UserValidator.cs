﻿using Curator.Core.API.Validation;
using Curator.Model;
using System.Collections.Generic;

namespace Curator.Core.Validation
{
    internal static class UserValidator
    {
        internal static ValidationResult ValidateForCreation(User user, string password)
        {
            var errors = new List<ValidationError>();
            if (user == null)
            {
                errors.Add(new ValidationError($"{nameof(user)} cannot be null"));
            }
            else
            {
                if (string.IsNullOrWhiteSpace(password))
                {
                    errors.Add(new ValidationError($"{nameof(password)} cannot be empty"));
                }
                if (string.IsNullOrWhiteSpace(user.Name))
                {
                    errors.Add(new ValidationError($"{nameof(user.Name)} cannot be empty"));
                }
                if (string.IsNullOrWhiteSpace(user.Username))
                {
                    errors.Add(new ValidationError($"{nameof(user.Username)} cannot be empty"));
                }
                if (string.IsNullOrWhiteSpace(user.Email))
                {
                    errors.Add(new ValidationError($"{nameof(user.Email)} cannot be empty"));
                }
            }
            return new ValidationResult { Errors = errors };
        }

        internal static ValidationResult ValidateLogin(string username, string password)
        {
            var errors = new List<ValidationError>();
            if (string.IsNullOrWhiteSpace(username))
            {
                errors.Add(new ValidationError($"{nameof(username)} cannot be empty"));
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                errors.Add(new ValidationError($"{nameof(password)} cannot be empty"));
            }
            return new ValidationResult { Errors = errors };
        }
    }
}
