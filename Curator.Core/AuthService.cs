﻿using Curator.Core.API;
using Curator.Core.Validation;
using Curator.Model;
using Curator.Persistence.API;
using System;
using System.Threading.Tasks;

namespace Curator.Core
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository repository;


        public AuthService(IAuthRepository repository)
        {
            this.repository = repository;
        }

        public async Task Register(User user, string password)
        {
            var validationResult = UserValidator.ValidateForCreation(user, password);
            validationResult.ThrowForError();

            await repository.Register(user, password);
        }

        public async Task<User> Login(string username, string password)
        {
            var validationResult = UserValidator.ValidateLogin(username, password);
            validationResult.ThrowForError();

            return await repository.Login(username, password);
        }
    }
}
