﻿using Curator.Core.API;
using Curator.Model;
using Curator.Persistence.API;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Curator.Core
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<User> FindById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            return await userRepository.FindById(id);
        }

        public async Task<Page<Product>> FindProducts(string userId, int skip, int take)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));

            // Look for one more item than requested so we can determine if there are more records
            var products = await userRepository.FindProducts(userId, skip, take + 1);
            return new Page<Product>
            {
                Results = products.Take(take).ToArray(),
                HasMore = products.Length > take
            };
        }
    }
}
